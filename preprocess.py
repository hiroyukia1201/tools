import glob
import os
import sys

import cv2
import numpy as np
import PySide2

from PySide2 import QtCore, QtGui, QtUiTools, QtWidgets



class PreProcessWindow(QtWidgets.QMainWindow):
    CURRENT_PATH = os.path.dirname(__file__)
    # 画像の保存先フォルダ
    FOLDER = "image"
    # 画像の拡張子
    EXTENSION = "jpg"
    # UIファイル
    UI_FILE_NAME = "preprocess.ui"
    # Window終了シグナル
    window_closed_signal = QtCore.Signal()

    def __init__(self, parent=None):
        super(PreProcessWindow, self).__init__(parent)
        
        # UIの初期化       
        self.window = self.initalize_ui()
        # UIの表示       
        self.show_window()
        # 元画像のリスト
        self.original_image_list = []
        # 検証用画像のリスト
        self.preprocessed_image_list = []
        # 画像読み込み用に画像番号を設定
        self.image_number = 0

        # ボタン押下時の動作を設定
        self.window.button_read_image.clicked.connect(self.read_image)
        self.window.button_execution.clicked.connect(self.execution)
        self.window.button_left.clicked.connect(self.button_left)
        self.window.button_right.clicked.connect(self.button_right)

        # スピンボックスやスライダーの値を変更した際に両方を連動させる
        self.window.spinBox_filtering.valueChanged.connect(self.window.horizontalSlider_filtering.setValue)
        self.window.spinBox_filtering.editingFinished.connect(self.check_value_filtering)
        self.window.horizontalSlider_filtering.valueChanged.connect(self.check_value_filtering)
        
        self.window.spinBox_thresholding.valueChanged.connect(self.window.horizontalSlider_thresholding.setValue)
        self.window.horizontalSlider_thresholding.valueChanged.connect(self.window.spinBox_thresholding.setValue)

        self.window.spinBox_Morphological_Operations.valueChanged.connect(self.window.horizontalSlider_Morphological_Operations.setValue)
        self.window.spinBox_Morphological_Operations.editingFinished.connect(self.check_value_Morphological_Operations)
        self.window.horizontalSlider_Morphological_Operations.valueChanged.connect(self.check_value_Morphological_Operations)
        
    def initalize_ui(self):
        """
        UI初期化処理
        """
        uifile = QtCore.QFile(os.path.join(self.CURRENT_PATH, self.UI_FILE_NAME))
        uifile.open(QtCore.QFile.ReadOnly)
        loader = QtUiTools.QUiLoader()
        window = loader.load(uifile)
        uifile.close()
        return window

    def show_window(self):
        """
        WINDOWを最大サイズで表示
        """
        self.window.setWindowModality(QtCore.Qt.ApplicationModal)
        self.window.showMaximized()

    def check_value_filtering(self):
        """
        平滑化のカーネルサイズが偶数になっていないかチェック
        偶数になっている場合は奇数に変更
        """
        slider_value = self.window.horizontalSlider_filtering.value()
        if slider_value % 2 == 0:
            self.window.spinBox_filtering.setValue(slider_value - 1)
            self.window.horizontalSlider_filtering.setValue(slider_value - 1)
        else:
            self.window.spinBox_filtering.setValue(slider_value)

    def check_value_Morphological_Operations(self):
        """
        モルフォロジー変換のカーネルサイズが偶数になっていないかチェック
        偶数になっている場合は奇数に変更
        """
        slider_value = self.window.horizontalSlider_Morphological_Operations.value()
        if slider_value % 2 == 0:
            self.window.spinBox_Morphological_Operations.setValue(slider_value - 1)
            self.window.horizontalSlider_Morphological_Operations.setValue(slider_value - 1)
        else:
            self.window.spinBox_Morphological_Operations.setValue(slider_value)

    def read_image(self):
        """
        フォルダから画像を取得
        """
        self.original_image_list = glob.glob(os.path.join(self.CURRENT_PATH, self.FOLDER, f"*.{self.EXTENSION}"))

        # フォルダに画像が保存されていない場合、エラーメッセージを表示
        if len(self.original_image_list) == 0:
            self.window.label_error.setText("フォルダに画像が保存されていません")
        else:
            # 画像を読み込み
            self.preprocessed_image_list = cv2.imread(self.original_image_list[self.image_number])
            # OpenCVの形式からQT用のイメージへ変換
            self.image_conversion(self.preprocessed_image_list, self.window.graphicsView_original)
            # エラーメッセージを初期化
            self.window.label_error.setText("")

    def execution(self):
        """
        画像を読み込み、前処理を実行
        """
        # 画像を読み込み
        self.read_image()
        # 処理順を読み込み、処理順に前処理を実行
        self.confirm_preprocess_order()
    
    def confirm_preprocess_order(self):
        """
        グループボックスにチェックが入っている前処理の処理順を取得し、
        指定された処理順に前処理を実施
        """
        # グループボックスにチェックが入っている場合に平滑化の処理順を取得
        if self.window.groupBox_filtering.isChecked():
            filtering_order = self.window.spinBox_filtering_order.value()
        else:
            filtering_order = None

        # グループボックスにチェックが入っている場合に二値化の処理順を取得
        if self.window.groupBox_thresholding.isChecked():
            thresholding_order = self.window.spinBox_thresholding_order.value()
        else:
            thresholding_order = None

        # グループボックスにチェックが入っている場合にモルフォロジー変換の処理順を取得
        if self.window.groupBox_Morphological_Operations.isChecked():
            Morphological_Operations_order = self.window.spinBox_Morphological_Operations_order.value()
        else:
            Morphological_Operations_order = None

        # 前処理手法と処理順が入った辞書を作成
        preprocess_order_all = {"filtering":filtering_order, "thresholding":thresholding_order, "Morphological_Operations":Morphological_Operations_order}   
        # グループボックスにチェックが入っていた前処理のみを抽出
        preprocess_order_selected = {}
        for key,value in preprocess_order_all.items():
            if value != None:
                preprocess_order_selected[key] =value

        # 処理順が重複していないかチェックし、重複している場合はエラーメッセージを表示
        if len(set(preprocess_order_selected.keys())) != len(set(preprocess_order_selected.values())):
            self.window.label_error.setText("処理順が重複しています")

        else:
            # 処理順に重複がない場合はエラーメッセージを初期化
            self.window.label_error.setText("")
            # 処理順にソート
            preprocess_order_selected = sorted(preprocess_order_selected.items(), key=lambda x:x[1])
            for i in range(len(preprocess_order_selected)):
                # グループボックスにチェックが入っている処理順に前処理を実行
                if "filtering" == preprocess_order_selected[i][0]:
                    self.filtering(self.preprocessed_image_list)
                elif "thresholding" == preprocess_order_selected[i][0]:
                    self.thresholding(self.preprocessed_image_list)
                elif "Morphological_Operations" == preprocess_order_selected[i][0]:
                    self.Morphological_Operations(self.preprocessed_image_list)
            # 前処理後の画像をグラフィックウィンドウに表示
            self.image_conversion(self.preprocessed_image_list, self.window.graphicsView_preprocessed)                     

    def filtering(self, image):
        """
        平滑化の処理を実施
        """
        # スピンボックスの値を取得し、カーネルサイズを設定
        kernel_size = self.window.spinBox_filtering.value()
        # 選択した平滑化の処理を実施
        if self.window.radioButton_MeanBlur.isChecked():
            image = cv2.blur(image,(kernel_size, kernel_size))
        elif self.window.radioButton_GaussianBlur.isChecked():
            image = cv2.GaussianBlur(image,(kernel_size, kernel_size), 0)
        elif self.window.radioButton_MedianBlur.isChecked():
            image = cv2.medianBlur(image, kernel_size)
        self.preprocessed_image_list = image

    def thresholding(self, image):
        """
        二値化の処理を実施
        """
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        # 選択した二値化の処理を実施
        if self.window.radioButton_threshold_normal.isChecked():
            # スピンボックスの値を取得し、閾値を設定
            threshold = self.window.spinBox_thresholding.value()
            _, image = cv2.threshold(image, threshold, 255, cv2.THRESH_BINARY)
        elif self.window.radioButton_threshold_Otsu.isChecked():        
            _, image = cv2.threshold(image, 0, 255, cv2.THRESH_OTSU)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        self.preprocessed_image_list = image

    def Morphological_Operations(self, image):
        """
        モルフォロジー変換の処理を実施
        """
        # スピンボックスの値を取得し、カーネルサイズを設定
        kernel_size = self.window.spinBox_Morphological_Operations.value()
        kernel = np.ones((kernel_size, kernel_size),np.uint8)
        # 選択したモルフォロジー変換の処理を実施
        if self.window.radioButton_Opening.isChecked():
            image = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
        elif self.window.radioButton_Closing.isChecked():
            image = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
        self.preprocessed_image_list = image
    
    def button_left(self):
        """
        左カーソルボタン押下時の処理を実施
        """
        # 左カーソルのボタンを押下時に一つ前の画像を読み込み、前処理を実行
        if self.image_number > 0:
            self.image_number -= 1
        self.execution()
        
    def button_right(self):
        """
        右カーソルボタン押下時の処理を実施
        """
        # 右カーソルのボタンを押下時に一つ後の画像を読み込み、前処理を実行
        if self.image_number < len(self.original_image_list)-1:
            self.image_number += 1
            self.execution()

    def image_conversion(self, image, view):
        """
        画像を表示するための処理を実施
        """
        # OpenCVのMat形式からQT用のイメージへ変換
        scene = QtWidgets.QGraphicsScene()
        cv_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        height, width, dim = cv_img.shape
        bytes_per_line = dim * width
        qt_img = QtGui.QImage(cv_img.data, width, height,
                              bytes_per_line, QtGui.QImage.Format_RGB888)
        item = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap.fromImage(qt_img))

        # QGraphicViewへ画像を設定
        scene.addItem(item)
        view.setScene(scene)

        # 画像のアスペクト比は維持してQGraphicViewへ表示する
        view.fitInView(QtCore.QRectF(0, 0, width, height),
                       QtCore.Qt.AspectRatioMode.KeepAspectRatio)   


if __name__ == "__main__":
    # PySide2ライブラリへのパスを登録
    dirname = os.path.dirname(PySide2.__file__)
    plugin_path = os.path.join(dirname, 'plugins', 'platforms')
    os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = plugin_path
    # Pyside2の実行
    app = QtWidgets.QApplication(sys.argv)
    # ダッシュボード画面の起動
    window = PreProcessWindow()
    app.exec_()